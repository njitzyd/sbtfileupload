package com.njitzyd.springbootfileupload.tools;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@Configuration
public class MyWebAppConfigurer implements WebMvcConfigurer {
	
	   @Value("${file.staticAccessPath}")
	    private String staticAccessPath;
	  
	   @Value("${file.uploadFolder}")
	    private String uploadFolder;
	
    @Override
    public void addResourceHandlers(ResourceHandlerRegistry registry) {

    	// 本地映射到远端
        registry.addResourceHandler(staticAccessPath).addResourceLocations(uploadFolder);
    }
}
